import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { HealthPage } from "../health/health";
import { ProfilePage } from "../profile/profile";
import { HospitalPage } from "../hospital/hospital";


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;
  tab4Root = HealthPage;
  tab5Root = ProfilePage;
  tab6Root = HospitalPage;
  constructor() {

  }
}
