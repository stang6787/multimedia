import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the HealthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-health',
  templateUrl: 'health.html',
})
export class HealthPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HealthPage');
  }

  slider = [
    {
      title:'Ionic is Awesome!',
      description:'Ionic is buit on top of Angula and Ionic is ',
      image:"./assets/imgs/CaptainM.svg"

    },
    {
      title:'Ionic is Awesome!',
      description:'Ionic is buit on top of Angula and Ionic is ',
      image:"./assets/imgs/maps.svg"
    },
  ];

}
